import React from "react"
import Header from "./components/Header"
import FormMeme from "./components/FormMeme"

export default function App() {
    return (
        <div>
            <Header />
            <FormMeme/>
            
        </div>
    )
}
